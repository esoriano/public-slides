Aquí publicamos las transparencias de la asignatura Laboratorio 
de Sistemas (primer curso de Ing. Robótica Software 
ETSIT, Universidad Rey Juan Carlos) para el uso y disfrute general. 

Para el estudio de la asignatura, se recomienda consultar la 
bibliografía:

**Bibliografía**

    How Linux Works, 2nd Edition,No Starch Press, Brian Ward 
    The UNIX Programming Environment, Prentice Hall, Brian W. Kernighan and Rob Pike
    UNIX power tools, O'Reilly, Mike Loukides, Tim O'Reilly, Jerry Peek, Shelley Powers,
    UNIX and Linux System Administration Handbook, Fifth Edition, Evi Nemeth et al.


Se pueden enviar dudas, sugerencias, erratas, etc. a:

   *enrique.soriano@urjc.es*
   *gorka.guardiola@urjc.es*

Licencia para todas las transparencias:

(CC) Algunos derechos reservados. Este trabajo se entrega bajo la
licencia Creative Commons Reconocimiento - NoComercial - 
SinObraDerivada (by-nc-nd).

Para obtener la licencia completa:

  http://creativecommons.org/licenses/by-sa/2.1/es. 

También puede solicitarse a:
  Creative Commons
  559 Nathan Abbott Way, Stanford,
  California 94305, USA.

Si deseas hacer obra derivada, ponte en contacto con nosotros.

