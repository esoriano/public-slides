Aquí dejo las transparencias que uso para impartir mi parte de la asignatura
Seguridad en Redes de Ordernadores (tercer curso de Ing. Telemática de la 
ETSIT, Universidad Rey Juan Carlos) para el uso y disfrute general. 

Se pueden enviar dudas, sugerencias, erratas, etc. a:

   *enrique.soriano@urjc.es*

Licencia para todas las transparencias:

(CC) Algunos derechos reservados. Este trabajo se entrega bajo la
licencia Creative Commons Reconocimiento - NoComercial - 
SinObraDerivada (by-nc-nd).

Para obtener la licencia completa:

  http://creativecommons.org/licenses/by-sa/2.1/es. 

También puede solicitarse a:
  Creative Commons
  559 Nathan Abbott Way, Stanford,
  California 94305, USA.

Si deseas hacer obra derivada, ponte en contacto con nosotros.

