#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <err.h>


int
main(int argc, char *argv[])
{
	int fd;

	fd=open("/tmp/a", O_WRONLY|O_CREAT|O_TRUNC, 0666);
	if(fd < 0)
		err(EXIT_FAILURE, "can't open");
	switch(fork()){
	case  -1:
		err(EXIT_FAILURE, "can't fork");
	case 0:
		/* descomentar para probar: como truncan el fichero,
		 * el resultado final puede ser solo lo que escribe
		 * el hijo, solo lo que escribe el padre, o la super
		 * posicion de ambas escrituras (en cualquier orden).
		 * ya que padre e hijo no comparten offset.
		 */
		
		//fd=open("/tmp/a", O_WRONLY|O_CREAT|O_TRUNC, 0666);
		if(write(fd, "HIJO\n", 5) != 5)
			err(EXIT_FAILURE, "can't write");
		break;
	default:
		
		//fd=open("/tmp/a", O_WRONLY|O_CREAT|O_TRUNC, 0666);
		if(write(fd, "PADRE\n", 6) != 6)
			err(EXIT_FAILURE, "can't write");
	}
	close(fd);
	exit(EXIT_SUCCESS);
}
