#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <err.h>

enum{
	Bufsize = 8* 1024,
};

int
main(int argc, char *argv[])
{
	int fd;
	char buf[Bufsize];
	int nr;
	int c=0;

	if(argc != 2)
		errx(EXIT_FAILURE,"usage: %s file", argv[0]);
	fd = open(argv[1], O_RDONLY);
	if(fd < 0)
		err(1, "can't open file");
	while((nr = read(fd, buf, Bufsize)) != 0){
		if(nr < 0)
			err(EXIT_FAILURE, "can't read");
		c++;
		if(write(1, buf, nr) != nr)
			err(EXIT_FAILURE, "can't write");
	}
	close(fd);
	fprintf(stdout, "number of iterations: %d\n", c);
	exit(EXIT_SUCCESS);
}
