#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/*
 * Trick: in order to make it fail, read from
 * /proc/self/mem, you will get an IO error.
 * In this case, it should be the stdin, so
 * try to read /proc/xxx/mem (another process with the same UID
 * (the open can fail, it depends on the ptrace stuff, try with
 * firefox).
 *
 * 	./a.out < /proc/xxx/mem
 */

enum{
	Maxline = 256,
};

int
main(int argc, char *argv[])
{
	char line[Maxline];

	while(fgets(line, Maxline, stdin) != NULL){
		printf("linea: %s\n", line);
	}
	if(! feof(stdin)){
		errx(EXIT_FAILURE, "eof not reached");
	}
	exit(EXIT_SUCCESS);
}
