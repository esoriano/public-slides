#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <err.h>

/*
 * creates a file with some size,  filled with zeros.
 */

int
main(int argc, char *argv[])
{
	int fd;
	int size;
	char c;

	if(argc != 3)
		errx(1, "usage: %s file size", argv[0]);

	fd = open(argv[1], O_RDWR|O_CREAT|O_TRUNC, 0640);
	if(fd < 0)
		err(1, "can't open file");

	size = atoi(argv[2]);
	if(size > 0){
		lseek(fd, size-1, SEEK_SET);
		c = 0;
		if(write(fd, &c ,1) != 1)
			err(EXIT_FAILURE, "can't write");
	}
	close(fd);
	exit(EXIT_SUCCESS);
}
