#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <err.h>
#include <stdlib.h>

/*
	
What does this program print?

gcc -o race -Wall -Wshadow  $%

while true ; do 	./race | grep -v "YYYYXXXX" ; done

*/

int
main(int argc, char *argv[])
{
	switch(fork()){
	case -1:
		err(EXIT_FAILURE, "fork failed!");
	case 0:
		printf("X");
		printf("X");
		printf("X");
		printf("X");
	 	break;
	default:
		printf("Y");
		printf("Y");
		printf("Y");
		printf("Y");
	}
	exit(EXIT_SUCCESS);
}
