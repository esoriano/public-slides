#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <err.h>
#include <stdlib.h>

/*

gcc -o fork0 -Wall -Wshadow  fork0.c

Execute  ./fork0-test.sh  

*/

int
main(int argc, char *argv[])
{
	printf("hello\n");
	switch(fork()){
	case -1:
		err(EXIT_FAILURE, "fork failed!");
	case 0:
		printf("I am the child\n");
		exit(EXIT_SUCCESS);
	default:
		printf("I am the parent\n");
		exit(EXIT_SUCCESS);
	}
}
