#include <stdio.h>
#include <unistd.h>
#include <err.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

/*

gcc -o wait -Wall -Wshadow wait.c

*/

enum{
	Nchildren = 5,
};

int
main(int argc, char *argv[])
{
	int pid;
	int sts;
	int i;
	for(i=0; i<Nchildren; i++){
		pid = fork();
		switch(pid){
		case -1:
			err(EXIT_FAILURE, "fork failed!");
		case 0:
			/*
			 * Note that it is just an example. This is
			 * not the correct way to sleep 10 seconds!
			 */
			execl("/bin/sleep", "sleep", "60", NULL);
			err(EXIT_FAILURE, "exec failed");
		default:
			printf("child created: %d\n", pid);
		}
	}

	while((pid = wait(&sts)) != -1){
		printf("Did process %d exit?\n", pid);
		if(WIFEXITED(sts)){
			printf("Yes! the status was: %d\n", WEXITSTATUS(sts));
		}else{
			printf("No\n");
		}
	}
	exit(EXIT_SUCCESS);
}
