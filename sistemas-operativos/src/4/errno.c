#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

/*
gcc -o errno -Wall -Wshadow $%
*/


int
main(int argc, char *argv[])
{
	char path[256];

	if(argc != 2){
		printf("usage: %s dir\n", argv[0]);		
		exit(EXIT_FAILURE);
	}
	if(chdir(argv[1]) < 0){
		printf("errno is %d\n", errno);		
		perror("can't change the working directory");
		exit(EXIT_FAILURE);
	}
	if(getcwd(path, 256) == NULL){
		printf("errno is %d\n", errno);
		perror("can't get the working directory");
		exit(EXIT_FAILURE);
	}
	printf("my working dir is %s\n", path);
	exit(EXIT_SUCCESS);
}

