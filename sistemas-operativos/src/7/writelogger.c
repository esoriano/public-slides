#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

enum{
	Maxevents = 10,
};

void
handler(int number)
{
	errx(EXIT_FAILURE, "killed by signal %d", number);
}

int
main(int argc, char *argv[])
{
	FILE *f;
	int i=0;
	int fd;

	signal(SIGPIPE, handler);

	fd = open("/tmp/logger", O_WRONLY|O_NONBLOCK);
	if(fd<0)
		err(EXIT_FAILURE, "cannot open /tmp/logger");
	f = fdopen(fd, "w");
	if(f == NULL)
		err(EXIT_FAILURE, "cannot open stream");
	for(i=0; i<Maxevents; i++){
		sleep(1);
		fprintf(f, "event #%d\n", i);
		fflush(f);
	}
	fclose(f);
	exit(EXIT_SUCCESS);
}
