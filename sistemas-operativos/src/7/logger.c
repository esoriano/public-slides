#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <err.h>
#include <dirent.h>
#include <time.h>
#include <signal.h>
#include <fcntl.h>

enum{
	Maxline=500,
};

void
printevent(char *e)
{
	time_t now;
	char *p;

	time(&now);
	p = ctime(&now);
	p[strlen(p)-1] = '\0';
 	printf("%s: %s", p, e);
}


int
main(int argc, char **argv)
{
	FILE *st;
	char line[Maxline];
 	int eof = 0;

	if(access("/tmp/logger", F_OK) == 0)
		if(unlink("/tmp/logger") < 0)
			err(1, "cant remove /tmp/logger");

	if(mkfifo("/tmp/logger", 0664) < 0)
		err(EXIT_FAILURE, "cannot make fifo /tmp/logger");

	for(;;){
 		printevent("waiting for clients\n");
		st = fopen("/tmp/logger", "r");
		printevent("ready to read events\n");
 		if(st != NULL){
			eof = 0;
 			while(! eof){
   				if(fgets(line, Maxline, st)  != NULL){
					printevent(line);
				}else{
					if(feof(st)){
						printevent("client is gone\n");
						eof = 1;
					}else{
						err(1, "error reading from fifo");
					}
				}
			}
 		}
		fclose(st);
	}
}
