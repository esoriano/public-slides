#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

void
handler(int number)
{
		fprintf(stderr, "I am the handler\n");
}

void
printpending()
{
	sigset_t pending;
	int i;

	sigemptyset(&pending);
	sigpending(&pending);
	fprintf(stderr, "Pending signals:\n");
	for(i=1; i<NSIG; i++)
		if(sigismember(&pending, i))
			fprintf(stderr, "%02d: yes \n", i );
		else
			fprintf(stderr, "%02d: no\n", i );
}

int
main(int argc, char *argv[])
{
	int i;
	sigset_t  set;

 	signal(SIGINT, handler);
	sigemptyset(&set);
	sigaddset(&set, SIGINT);
	sigprocmask(SIG_BLOCK, &set, NULL);

	fprintf(stderr, "Blocking SIGINT for 10 seconds");
	for(i=0; i<10; i++){
		sleep(1);
		fprintf(stderr, ".");
	}
	fprintf(stderr, "\n" );

	printpending();

	fprintf(stderr, "\nSIGINT is unblocked\n");
	sigprocmask(SIG_UNBLOCK, &set, NULL);

	sleep(10);
	fprintf(stderr, "main done, bye bye!\n");
	exit(EXIT_SUCCESS);
}
