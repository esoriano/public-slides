#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

enum{
		Bsize = 1024,
};

void
handler(int number)
{
	fprintf(stderr, "this is the handler, is the syscall restarted?\n");
}

int
main(int argc, char *argv[])
{
	char buf[Bsize];
	int nr;

	signal(SIGALRM, handler);

	// comment to enable slow syscall autorestart
	siginterrupt(SIGALRM, 1);

	alarm(5);
	nr = read(0, buf, sizeof(buf)-1);
	alarm(0);
	if(nr <  0){
		if(errno == EINTR)
			fprintf(stdout, "timeout!\n");
		else
			err(EXIT_FAILURE, "cant read");
	}else{
		buf[nr] = '\0';
		printf("data: %s\n", buf);
	}
	exit(EXIT_SUCCESS);
}
