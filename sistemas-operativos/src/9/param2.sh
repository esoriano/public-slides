#!/bin/sh

## ./param2.sh uno dos tres "cuatro cinco"


echo "Using \"\$*\":"
for a in "$*"
do
    echo $a;
done

echo
echo "Using \$*:"
for a in $*
do
    echo $a;
done

echo
echo "Using \"\$@\":"
for a in "$@"
do
    echo $a;
done

echo
echo "Using \$@:"
for a in $@
do
    echo $a;
done  
