#!/bin/bash

usage(){
	echo "usage: ldown [num]" 1>&2;
	exit 1
}

case $# in
0)
	num=1
	;;
1)
	num=$1
	shift
	if echo $num |grep -Ev '[0-9]+' 2>&1 > /dev/null; then
		usage
	fi
	;;
*)
	usage
esac

echo $num
