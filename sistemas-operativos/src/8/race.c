#define _GNU_SOURCE
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <err.h>
#include <unistd.h>

/*
	gcc  -o race -Wall -Wshadow -g race.c -lpthread
	./race | egrep '^.$'
*/

enum{
	Nrounds = 1000000,
	Prob = 25,
	Count = 10,
};

#define  FUSS   if(rand()%100 < Prob) usleep(100);

int x;

void *fn(void *p)
{
	int i, aux;

	for(i=0; i<Count; i++){
		aux = x;
		FUSS
		aux = aux + 1;
		x = aux;
	}
  	return NULL;
}

int
main(int argc, char *argv[])
{
	int i;
	pthread_t  thread1, thread2;

	for(i=0; i<Nrounds; i++){
		x = 0;
		if(pthread_create(&thread1, NULL, fn, NULL)) {
			warn("error creating thread");
			return 1;
		}
		if(pthread_create(&thread2, NULL, fn, NULL)) {
			warn("error creating thread");
			return 1;
		}
		if(pthread_join(thread1, NULL)  != 0){
			warn("error joining thread");
			return 1;
		}
		if(pthread_join(thread2, NULL)  != 0){
			warn("error joining thread");
			return 1;
		}
		printf("%d\n", x);
	}
	return 0;
}
