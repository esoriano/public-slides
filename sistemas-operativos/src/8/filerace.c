#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/file.h>

/*
	GOAL:

	* The processes have to write  ALL their messages.

	* The file does not exist when the processes are
		created.

	* The messages of a process must be CONTIGUOUS
		in the file.
*/

enum{
	Nprocs = 10,
	Maxstr = 256,
	Nrounds = 5,
};

void
dowrite(int fd)
{
	char str[Maxstr];
	int i;

	for(i=0; i<Nrounds; i++){
		snprintf(str, Maxstr, "PID %d was here! round %d\n",
		 	getpid(), i);
		if(write(fd, str, strlen(str)) != strlen(str))
			err(EXIT_FAILURE, "can't write file");
	}
}

void
okchild(char *path)
{
	int fd;

	/*
	 * append_only is needed because each fd has its own offset (at 0).
	 */
	fd = open(path, O_RDWR|O_CREAT|O_APPEND, 0666);
	if(fd < 0)
 		err(EXIT_FAILURE, "can't create file %s", path);
	flock(fd, LOCK_EX);
	dowrite(fd);
 	flock(fd, LOCK_UN);
	close(fd);
}

void
wrongchild(char *path)
{
 	int fd;

	fd = open(path, O_RDWR|O_CREAT, 0666);
	if(fd < 0)
 		err(EXIT_FAILURE, "can't create file %s", path);
 	dowrite(fd);
	close(fd);
}

int
main(int argc, char *argv[])
{
	int i;

	if(argc != 2)
		errx(EXIT_FAILURE, "usage: %s file", argv[0]);

	unlink(argv[1]);

	for(i=0; i<Nprocs; i++){
		switch(fork()){
		case -1:
				err(EXIT_FAILURE, "can't fork");
		case 0:
				okchild(argv[1]);
				exit(EXIT_SUCCESS);
		}
	}
 	while(wait(NULL) != -1)
		fprintf(stderr, ".");
	fprintf(stderr, "\n");
	exit(EXIT_SUCCESS);
}
