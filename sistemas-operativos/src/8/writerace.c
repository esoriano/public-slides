#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <err.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>

enum{
	Nrounds = 100,
	Nprocs = 10,
	Maxstr = 256,
};

void
child(int fd, int index)
{
	int i;
	char str[Maxstr];

	for(i=0; i< Nrounds; i++){
		snprintf(str, Maxstr, "%d", index);
		if(write(fd, "[", 1) != 1)
			err(EXIT_FAILURE,"write failed");
		if(write(fd, str, strlen(str)) != strlen(str))
			err(EXIT_FAILURE,"write failed");
		if(write(fd, "]", 1) != 1)
			err(EXIT_FAILURE,"write failed");
	}
	close(fd);
}

int
main(int argc, char *argv[])
{
	int fd;
	int i;

	if(argc != 2)
		errx(EXIT_FAILURE, "usage: %s file", argv[0]);
	fd = open(argv[1], O_RDWR|O_CREAT, 0666);
	if(fd < 0)
		err(EXIT_FAILURE, "can't open file %s", argv[1]);

	for(i=0; i<Nprocs; i++){
		switch(fork()){
		case -1:
			err(EXIT_FAILURE, "can't fork");
		case 0:
			child(fd, i);
			exit(EXIT_SUCCESS);
		}
	}
	close(fd);
	while(wait(NULL) != -1)
		;
	exit(EXIT_SUCCESS);
}
