Aquí dejo las transparencias que uso para impartir la asignatura
de Sistemas Operativos (tercer curso de Ing. Telemática de la 
ETSIT, Universidad Rey Juan Carlos) para el uso y disfrute general. 

En el directorio src tienes algunos de los programas que implemento
en clase como ejemplos de uso de llamadas al sistema y funciones 
de la libc, y algunos scripts de shell. Hay un subdirectorio para cada 
tema. 

Para el estudio de la asignatura, se recomienda consultar la 
bibliografía:

**Bibliografía**

    Brian W. Kernighan, Dennis Ritchie, Prentice Hall, The C programming language
    Andrew S. Tanenbaum, Prentice Hall, Modern Operating Systems
    Mike Loukides, Tim O'Reilly, Jerry Peek, Shelley Powers, O'Reilly, Unix Power Tools, 3rd Edition
    Brian W. Kernighan, Rob Pike, Prentice Hall, The Unix Programming Environment.


**Bibliografía de consulta**

    Richard Stevens, Addison-Wesley, Advanced Programming in the Unix Environment
    Brian W. Kernighan, Rob Pike, Addison-Wesley, Inc, The Practice of Programming
    Andrew S. Tanenbaum and Albert Woodhull, Prentice Hall, Operating Systems: Design and Implementation
    Daniel Bovet, Marco Cesati, Understanding the Linux Kernel, 3rd edition.

Se pueden enviar dudas, sugerencias, erratas, etc. a:

   *enrique.soriano@urjc.es*

Licencia para todas las transparencias:

(CC) Algunos derechos reservados. Este trabajo se entrega bajo la
licencia Creative Commons Reconocimiento - NoComercial - 
SinObraDerivada (by-nc-nd).

Para obtener la licencia completa:

  http://creativecommons.org/licenses/by-sa/2.1/es. 

También puede solicitarse a:
  Creative Commons
  559 Nathan Abbott Way, Stanford,
  California 94305, USA.

Si deseas hacer obra derivada, ponte en contacto con nosotros.

