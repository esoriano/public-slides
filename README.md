Aquí se pueden encontrar las transparencias que uso para impartir distintas asignaturas  y cursos.

Se pueden enviar dudas, sugerencias, erratas, etc. a:

*enrique.soriano@urjc.es*

(CC) Algunos derechos reservados. Este trabajo se entrega bajo la licencia Creative Commons Reconocimiento - NoComercial - SinObraDerivada (by-nc-nd).

Para obtener la licencia completa:

http://creativecommons.org/licenses/by-sa/2.1/es.

También puede solicitarse a: Creative Commons 559 Nathan Abbott Way, Stanford, California 94305, USA.

Si deseas hacer obra derivada, ponte en contacto con nosotros.


